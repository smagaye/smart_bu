-- Insertion dans livre
START TRANSACTION;
INSERT INTO `db_bu`.`livre`(
	`id`,
	`slug`,
	`titre`,
	`auteurs`,
	`description`,
	`image`,
	`small_image`,
	`code_langue`,
	`annee_publication`
)
select 
	substr(concat ("LIV",CURRENT_TIMESTAMP()+1,cast(book_id as CHAR),"1234567890") ,1,30),
	id,
	title,
	authors,
	original_title,
	image_url,
    small_image_url,
	language_code,
	original_publication_year
    from wiki_people.books
    ;
COMMIT;


-- Insertion dans la table utilisateur
START TRANSACTION;
INSERT INTO `db_bu`.`utilisateur`(
	`id`,
    `slug`,
	`prenom`,
	`nom`
)

SELECT 
	 substr(concat ("USR",CURRENT_TIMESTAMP()+1,cast(id as CHAR),"1234567890") ,1,23),	
     id,
    `user`.`last_name`,
    `user`.`first_name`
FROM `wiki_people`.`user`
;
COMMIT;


-- Insertion dans la table pret
truncate `db_bu`.`pret`;
START TRANSACTION;
INSERT INTO `db_bu`.`pret`(
	`slug_livre`,
	`slug_utilisateur`
)

SELECT
	concat(
		"INSERT INTO `db_bu`.`pret`(`slug_livre`,`slug_utilisateur`) VALUES (",book_id,",",user_id,");"
    )
FROM `wiki_people`.`to_read` order by book_id ASC
;
COMMIT;

rollback;
select * from db_bu.livre where slug=7517;

select * from wiki_people.to_read where book_id=741;

select * from db_bu.utilisateur where slug=69;
