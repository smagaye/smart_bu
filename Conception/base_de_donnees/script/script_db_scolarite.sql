-- MySQL Script generated by MySQL Workbench
-- Mon Jul  1 17:01:36 2019
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema db_scolarite
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `db_scolarite` ;

-- -----------------------------------------------------
-- Schema db_scolarite
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_scolarite` DEFAULT CHARACTER SET utf8 ;
USE `db_scolarite` ;

-- -----------------------------------------------------
-- Table `db_scolarite`.`etudiant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_scolarite`.`etudiant` ;

CREATE TABLE IF NOT EXISTS `db_scolarite`.`etudiant` (
  `id` INT NOT NULL,
  `numero_carte` VARCHAR(10) NOT NULL,
  `ine` VARCHAR(25) NULL,
  `prenom` VARCHAR(45) NULL,
  `nom` VARCHAR(45) NULL,
  `nationalite` VARCHAR(26) NULL,
  `email` VARCHAR(45) NULL,
  `email_institutionnel` VARCHAR(45) NULL,
  `telephone` VARCHAR(20) NULL,
  `adresse` VARCHAR(195) NULL,
  `date_naissance` DATE NULL,
  `lieu_naisssance` VARCHAR(195) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `numero_carte_UNIQUE` (`numero_carte` ASC),
  UNIQUE INDEX `email_institutionnel_UNIQUE` (`email_institutionnel` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  UNIQUE INDEX `ine_UNIQUE` (`ine` ASC),
  UNIQUE INDEX `telephone_UNIQUE` (`telephone` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_scolarite`.`departement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_scolarite`.`departement` ;

CREATE TABLE IF NOT EXISTS `db_scolarite`.`departement` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_scolarite`.`filiere`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_scolarite`.`filiere` ;

CREATE TABLE IF NOT EXISTS `db_scolarite`.`filiere` (
  `id` INT NOT NULL,
  `departement` INT NOT NULL,
  `nom` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nom_UNIQUE` (`nom` ASC),
  UNIQUE INDEX `departement_UNIQUE` (`departement` ASC),
  CONSTRAINT `fk_filiere_departement`
    FOREIGN KEY (`departement`)
    REFERENCES `db_scolarite`.`departement` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_scolarite`.`niveau`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_scolarite`.`niveau` ;

CREATE TABLE IF NOT EXISTS `db_scolarite`.`niveau` (
  `id` INT NOT NULL,
  `filiere` INT NOT NULL,
  `nom` VARCHAR(20) NULL,
  `code` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `filiere_UNIQUE` (`filiere` ASC),
  CONSTRAINT `fk_filiere_niveau`
    FOREIGN KEY (`filiere`)
    REFERENCES `db_scolarite`.`filiere` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `db_scolarite`.`inscription_pedagogique`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_scolarite`.`inscription_pedagogique` ;

CREATE TABLE IF NOT EXISTS `db_scolarite`.`inscription_pedagogique` (
  `etudiant` INT NOT NULL,
  `niveau` INT NOT NULL,
  `annee_academique` INT NOT NULL,
  `date_inscription` DATETIME NULL,
  `status` VARCHAR(45) NULL,
  PRIMARY KEY (`etudiant`, `niveau`, `annee_academique`),
  INDEX `fk_niveau_inscription_idx` (`niveau` ASC),
  CONSTRAINT `fk_etudiant_inscription`
    FOREIGN KEY (`etudiant`)
    REFERENCES `db_scolarite`.`etudiant` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_niveau_inscription`
    FOREIGN KEY (`niveau`)
    REFERENCES `db_scolarite`.`niveau` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
