package sn.ucad.master2.bo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Utilisateur {
    @ApiModelProperty(notes = "email de l'utilisateur") private String email;
    @JsonIgnoreProperties(value = { "id"}) private String id;
    @JsonIgnoreProperties(value = { "motPasse"}) private String motPasse;
    @ApiModelProperty(notes = "le numéro l'utilisateur") private Integer slug;
    @ApiModelProperty(notes = "le prénom de l'utilisateur") private String prenom;
    @ApiModelProperty(notes = "le numéro de téléphone de l'utilisateur") private String telephone;
    @ApiModelProperty(notes = "le nom de l'utilisateur") private String nom;
    @ApiModelProperty(notes = "lieu de l'utilisateur")private String lieuNaissance;
    @ApiModelProperty(notes = "l'adresse de l'utilisateur")private String adresse;
    @ApiModelProperty(notes = "la date de naissance de l'utilisateur")private Date dateNaissance;
    @ApiModelProperty(notes = "la nationalité de l'utilisateur")private String nationalite;
    @ApiModelProperty(notes = "le profile de l'utilisateur")private String profile;
    @ApiModelProperty(notes = "le nom de la photo de l'utilisateur")private String photo;
    @Basic
    @Column(name = "id")
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "slug",unique = true, nullable = false)
    public Integer getSlug() {
        return this.slug;
    }
    public void setSlug(Integer slug) {
        this.slug = slug;
    }
    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return this.prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    @Basic
    @Column(name = "nom")
    public String getNom() {
        return this.nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    @Basic
    @Column(name = "email")
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    @Basic
    @Column(name = "mot_passe")
    public String getMotPasse() {
        return this.motPasse;
    }
    public void setMotPasse(String motPasse) {
        this.motPasse = motPasse;
    }
    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return this.adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    @Basic
    @Column(name = "telephone")
    public String getTelephone() {
        return this.telephone;
    }
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    @Basic
    @Column(name = "date_naissance")
    public Date getDateNaissance() {
        return this.dateNaissance;
    }
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    @Basic
    @Column(name = "lieu_naissance")
    public String getLieuNaissance() {
        return this.lieuNaissance;
    }
    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }
    @Basic
    @Column(name = "nationalite")
    public String getNationalite() {
        return this.nationalite;
    }
    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }
    @Basic
    @Column(name = "profile")
    public String getProfile() {
        return this.profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }
    @Basic
    @Column(name = "photo")
    public String getPhoto() {
        return this.photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
}