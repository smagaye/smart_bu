package sn.ucad.master2.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Pret {
    private Integer id;
    private Integer slugLivre;
    private Integer slugUtilisateur;
    private Date datePret;
    private Date dateRetourPrevisionnelle;
    private Date dateRetourEffective;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "slug_livre")
    public Integer getSlugLivre() {
        return slugLivre;
    }

    public void setSlugLivre(Integer slugLivre) {
        this.slugLivre = slugLivre;
    }

    @Basic
    @Column(name = "slug_utilisateur")
    public Integer getSlugUtilisateur() {
        return slugUtilisateur;
    }

    public void setSlugUtilisateur(Integer slugUtilisateur) {
        this.slugUtilisateur = slugUtilisateur;
    }

    @Basic
    @Column(name = "date_pret")
    public Date getDatePret() {
        return datePret;
    }

    public void setDatePret(Date datePret) {
        this.datePret = datePret;
    }

    @Basic
    @Column(name = "date_retour_previsionnelle")
    public Date getDateRetourPrevisionnelle() {
        return dateRetourPrevisionnelle;
    }

    public void setDateRetourPrevisionnelle(Date dateRetourPrevisionnelle) {
        this.dateRetourPrevisionnelle = dateRetourPrevisionnelle;
    }

    @Basic
    @Column(name = "date_retour_effective")
    public Date getDateRetourEffective() {
        return dateRetourEffective;
    }

    public void setDateRetourEffective(Date dateRetourEffective) {
        this.dateRetourEffective = dateRetourEffective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pret pret = (Pret) o;
        return Objects.equals(id, pret.id) &&
                Objects.equals(slugLivre, pret.slugLivre) &&
                Objects.equals(slugUtilisateur, pret.slugUtilisateur) &&
                Objects.equals(datePret, pret.datePret) &&
                Objects.equals(dateRetourPrevisionnelle, pret.dateRetourPrevisionnelle) &&
                Objects.equals(dateRetourEffective, pret.dateRetourEffective);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, slugLivre, slugUtilisateur, datePret, dateRetourPrevisionnelle, dateRetourEffective);
    }
}
