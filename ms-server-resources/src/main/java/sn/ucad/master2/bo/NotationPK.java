package sn.ucad.master2.bo;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class NotationPK implements Serializable {
    private Integer slugLivre;
    private Integer slugUtilisateur;

    @Column(name = "slug_livre")
    @Id
    public Integer getSlugLivre() {
        return slugLivre;
    }

    public void setSlugLivre(Integer slugLivre) {
        this.slugLivre = slugLivre;
    }

    @Column(name = "slug_utilisateur")
    @Id
    public Integer getSlugUtilisateur() {
        return slugUtilisateur;
    }

    public void setSlugUtilisateur(Integer slugUtilisateur) {
        this.slugUtilisateur = slugUtilisateur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotationPK that = (NotationPK) o;
        return Objects.equals(slugLivre, that.slugLivre) &&
                Objects.equals(slugUtilisateur, that.slugUtilisateur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slugLivre, slugUtilisateur);
    }
}
