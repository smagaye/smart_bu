package sn.ucad.master2.bo;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Livre {
    private String id;
    private Integer slug;
    private String titre;
    private String auteurs;
    private String description;
    private String image;
    private String smallImage;
    private String codeLangue;
    private Integer anneePublication;
    private Byte disponible;

    @Basic
    @Column(name = "id",unique = true, nullable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "slug",unique = true, nullable = false)
    public Integer getSlug() {
        return slug;
    }

    public void setSlug(Integer slug) {
        this.slug = slug;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "auteurs")
    public String getAuteurs() {
        return auteurs;
    }

    public void setAuteurs(String auteurs) {
        this.auteurs = auteurs;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "small_image")
    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    @Basic
    @Column(name = "code_langue")
    public String getCodeLangue() {
        return codeLangue;
    }

    public void setCodeLangue(String codeLangue) {
        this.codeLangue = codeLangue;
    }

    @Basic
    @Column(name = "annee_publication")
    public Integer getAnneePublication() {
        return anneePublication;
    }

    public void setAnneePublication(Integer anneePublication) {
        this.anneePublication = anneePublication;
    }

    @Basic
    @Column(name = "disponible")
    public Byte getDisponible() {
        return disponible;
    }

    public void setDisponible(Byte disponible) {
        this.disponible = disponible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Livre livre = (Livre) o;
        return Objects.equals(id, livre.id) &&
                Objects.equals(slug, livre.slug) &&
                Objects.equals(titre, livre.titre) &&
                Objects.equals(auteurs, livre.auteurs) &&
                Objects.equals(description, livre.description) &&
                Objects.equals(image, livre.image) &&
                Objects.equals(smallImage, livre.smallImage) &&
                Objects.equals(codeLangue, livre.codeLangue) &&
                Objects.equals(anneePublication, livre.anneePublication) &&
                Objects.equals(disponible, livre.disponible);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, slug, titre, auteurs, description, image, smallImage, codeLangue, anneePublication, disponible);
    }
}
