package sn.ucad.master2.bo;

import javax.persistence.*;
import java.util.Objects;

@Entity
@IdClass(NotationPK.class)
public class Notation {
    private Integer slugLivre;
    private Integer slugUtilisateur;
    private Integer note;
    private String commentaire;

    @Id
    @Column(name = "slug_livre")
    public Integer getSlugLivre() {
        return slugLivre;
    }

    public void setSlugLivre(Integer slugLivre) {
        this.slugLivre = slugLivre;
    }

    @Id
    @Column(name = "slug_utilisateur")
    public Integer getSlugUtilisateur() {
        return slugUtilisateur;
    }

    public void setSlugUtilisateur(Integer slugUtilisateur) {
        this.slugUtilisateur = slugUtilisateur;
    }

    @Basic
    @Column(name = "note")
    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    @Basic
    @Column(name = "commentaire")
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notation notation = (Notation) o;
        return Objects.equals(slugLivre, notation.slugLivre) &&
                Objects.equals(slugUtilisateur, notation.slugUtilisateur) &&
                Objects.equals(note, notation.note) &&
                Objects.equals(commentaire, notation.commentaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slugLivre, slugUtilisateur, note, commentaire);
    }
}
