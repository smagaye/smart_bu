package sn.ucad.master2.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Etudiant {
    private String utilisateur;
    private String numeroCarte;

    @Id
    @Column(name = "utilisateur")
    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Basic
    @Column(name = "numero_carte")
    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Etudiant etudiant = (Etudiant) o;
        return Objects.equals(utilisateur, etudiant.utilisateur) &&
                Objects.equals(numeroCarte, etudiant.numeroCarte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(utilisateur, numeroCarte);
    }
}
