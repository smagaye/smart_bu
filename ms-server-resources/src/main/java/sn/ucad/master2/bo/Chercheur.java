package sn.ucad.master2.bo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Chercheur {
    private String utilisateur;
    private String libelle;
    private Integer anneeRecherche;
    private String universite;

    @Id
    @Column(name = "utilisateur")
    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Basic
    @Column(name = "annee_recherche")
    public Integer getAnneeRecherche() {
        return anneeRecherche;
    }

    public void setAnneeRecherche(Integer anneeRecherche) {
        this.anneeRecherche = anneeRecherche;
    }

    @Basic
    @Column(name = "universite")
    public String getUniversite() {
        return universite;
    }

    public void setUniversite(String universite) {
        this.universite = universite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chercheur chercheur = (Chercheur) o;
        return Objects.equals(utilisateur, chercheur.utilisateur) &&
                Objects.equals(libelle, chercheur.libelle) &&
                Objects.equals(anneeRecherche, chercheur.anneeRecherche) &&
                Objects.equals(universite, chercheur.universite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(utilisateur, libelle, anneeRecherche, universite);
    }
}
