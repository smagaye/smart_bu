package sn.ucad.master2.bo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@IdClass(InscriptionPK.class)
public class Inscription {
    private String utilisateur;
    private Integer anneeDebut;
    private Integer anneeFin;
    private Timestamp dateInscription;
    private Byte statut;

    @Id
    @Column(name = "utilisateur")
    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Id
    @Column(name = "annee_debut")
    public Integer getAnneeDebut() {
        return anneeDebut;
    }

    public void setAnneeDebut(Integer anneeDebut) {
        this.anneeDebut = anneeDebut;
    }

    @Id
    @Column(name = "annee_fin")
    public Integer getAnneeFin() {
        return anneeFin;
    }

    public void setAnneeFin(Integer anneeFin) {
        this.anneeFin = anneeFin;
    }

    @Basic
    @Column(name = "date_inscription")
    public Timestamp getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(Timestamp dateInscription) {
        this.dateInscription = dateInscription;
    }

    @Basic
    @Column(name = "statut")
    public Byte getStatut() {
        return statut;
    }

    public void setStatut(Byte statut) {
        this.statut = statut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Inscription that = (Inscription) o;
        return Objects.equals(utilisateur, that.utilisateur) &&
                Objects.equals(anneeDebut, that.anneeDebut) &&
                Objects.equals(anneeFin, that.anneeFin) &&
                Objects.equals(dateInscription, that.dateInscription) &&
                Objects.equals(statut, that.statut);
    }

    @Override
    public int hashCode() {
        return Objects.hash(utilisateur, anneeDebut, anneeFin, dateInscription, statut);
    }
}
