package sn.ucad.master2.bo;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class InscriptionPK implements Serializable {
    private String utilisateur;
    private Integer anneeDebut;
    private Integer anneeFin;

    @Column(name = "utilisateur")
    @Id
    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    @Column(name = "annee_debut")
    @Id
    public Integer getAnneeDebut() {
        return anneeDebut;
    }

    public void setAnneeDebut(Integer anneeDebut) {
        this.anneeDebut = anneeDebut;
    }

    @Column(name = "annee_fin")
    @Id
    public Integer getAnneeFin() {
        return anneeFin;
    }

    public void setAnneeFin(Integer anneeFin) {
        this.anneeFin = anneeFin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InscriptionPK that = (InscriptionPK) o;
        return Objects.equals(utilisateur, that.utilisateur) &&
                Objects.equals(anneeDebut, that.anneeDebut) &&
                Objects.equals(anneeFin, that.anneeFin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(utilisateur, anneeDebut, anneeFin);
    }
}
