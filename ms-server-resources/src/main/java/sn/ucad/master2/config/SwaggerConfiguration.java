package sn.ucad.master2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class SwaggerConfiguration{

    @Bean
    public Docket apiDocket() {

        Docket docket =  new Docket(DocumentationType.SWAGGER_2);
        docket.select()
                .apis(RequestHandlerSelectors.basePackage("sn.ucad.master2.controllers"))
                .paths(PathSelectors.any())
                .build().apiInfo(apiEndPointsInfo());

        return docket;

    }


    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Bibliothèque universitaire API")
                .description(
                        "La bibliothèque universitaire vous propose cette API RestFull qui permet\n" +
                        "de consulter les ressources en ligne. Elle  offre ainsi la possibilité de\n" +
                        "s'inscrire en ligne. Pour utiliser les différentes fonctionnalité de l'API,\n" +
                                "il faudra avoir un compte valide sur la plateforme smart_bu"
                )
                .contact(new Contact("Serigne Malick Ada Gaye", "www.aarujob.com", "serignemalickada@gmail.com"))
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}
