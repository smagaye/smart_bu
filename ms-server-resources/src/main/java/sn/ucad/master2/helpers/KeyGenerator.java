package sn.ucad.master2.helpers;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.StringTokenizer;

public interface KeyGenerator {

    public static String id(String prefixe) {
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        StringTokenizer dateSale = new StringTokenizer(now.toString(), "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ:.,/!?\\-_");
        StringBuilder dateUTC = new StringBuilder(prefixe);
        while (dateSale.hasMoreTokens()) {
            dateUTC.append(dateSale.nextToken());
        }
        return dateUTC.append("000").substring(0, 20);
    }
}
