package sn.ucad.master2.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.ucad.master2.bo.Utilisateur;

import java.util.List;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur,Integer> {

    @Override
    public <S extends Utilisateur> S save(S s);

    @Override
    public List<Utilisateur> findAll();

    @Override
    public void delete(Utilisateur utilisateur);

    @Override
    public void deleteById(Integer integer);

    public Utilisateur findById(String s);

    public Utilisateur findBySlug(int slug);

    public Utilisateur findBySlugOrTelephoneOrEmail(int slug, String telephone, String email);

    public Utilisateur findByEmailAndMotPasse(String login, String motPasse);
}
