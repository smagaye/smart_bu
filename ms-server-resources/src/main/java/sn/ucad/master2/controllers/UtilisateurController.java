package sn.ucad.master2.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.ucad.master2.bo.Utilisateur;
import sn.ucad.master2.dto.Dto;
import sn.ucad.master2.dto.beans.UtilisateurAPI;
import sn.ucad.master2.services.GenericService;
import sn.ucad.master2.services.UtilisateurService;

import java.util.List;
import java.util.Map;


@RepositoryRestController
@RequestMapping(path = "/smart_bu")
@Api(value = "API pour tester le serveur ressource")
public class UtilisateurController {

    @Autowired
    private GenericService genericService;

    @Autowired
    private UtilisateurService utilisateurService;

    @ApiOperation(value = "Liste des utilisateurs",response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Liste récupérée avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @GetMapping(path ="/utilisateurs")
    @Dto(UtilisateurAPI.class)
    public ResponseEntity<List<Utilisateur>> findAll(@RequestHeader Map<String, String> headers) {
        return utilisateurService.findAll(headers);
    }

    @ApiOperation(value = "Recupération d'un utilisateur selon l'identifiant fournit en paramètre",response = UtilisateurAPI.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objet récupéré avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @GetMapping(path ="/utilisateurs/{id}")
    @Dto(UtilisateurAPI.class)
    public ResponseEntity<Utilisateur> findUtilisateurById(@PathVariable("id") String id, @RequestHeader Map<String, String> headers) {
        return utilisateurService.findById(id,headers);
    }

    @ApiOperation(value = "Recupération d'un utilisateur selon le nom d'utilisateur et le mot de passe fournit en paramètre",response = UtilisateurAPI.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objet récupéré avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @PostMapping(path ="/utilisateurs/login")
    @Dto(UtilisateurAPI.class)
    public ResponseEntity<Utilisateur> findUtilisateurByloginandMotPasse(@RequestParam(name = "login") String login,@RequestParam(name = "motPasse") String motPasse, @RequestHeader Map<String, String> headers) {
        return utilisateurService.findByloginAndMotPassse(login,motPasse,headers);
    }

    @ApiOperation(value = "Création la ressource utilisateur fournit en paramètre",response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objet Créé avec succès"),
            @ApiResponse(code = 409, message = "La ressource existe déjà"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @PostMapping(path ="/utilisateurs/create")
    public ResponseEntity createUtilisateur(@RequestHeader Map<String,String> headers, @RequestBody UtilisateurAPI utilisateur){
        return utilisateurService.createUtilisateur(utilisateur,headers);
    }

    @ApiOperation(value = "Mise à jour de de la ressource utilisateur fournit en paramètre",response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objet modifié avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @PutMapping(path ="/utilisateurs/update")
    public ResponseEntity updateUtilisateur(@RequestHeader Map<String,String> headers, @RequestBody UtilisateurAPI utilisateurAPI){
        return utilisateurService.updateUtilisateur(utilisateurAPI,headers);
    }

    @ApiOperation(value = "Suppression un utilisateur selon l'identifiant fournit en paramètre",response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Objet supprimé avec succès"),
            @ApiResponse(code = 401, message = "Vous n'êtes pas autorisé à voir la ressource"),
            @ApiResponse(code = 403, message = "L'accès à la ressource que vous tentiez d'atteindre est interdit"),
            @ApiResponse(code = 404, message = "La ressource que vous tentiez d'atteindre est introuvable.")
    })
    @DeleteMapping(path ="/utilisateurs/{id}")
    public ResponseEntity deleteUtilisateurById(@PathVariable("id") String id, @RequestHeader Map<String, String> headers) {
        return utilisateurService.deleteById(id,headers);
    }

}
