package sn.ucad.master2.services;

import org.springframework.http.ResponseEntity;
import sn.ucad.master2.bo.Utilisateur;
import sn.ucad.master2.dto.beans.UtilisateurAPI;

import java.util.List;
import java.util.Map;

public interface UtilisateurService {

    public void delete (Utilisateur utilisateur);

    public void delete(int slug);

    public ResponseEntity<Utilisateur> findByPk(String id, Map<String, String> headers);

    public ResponseEntity<Utilisateur>findById(String slug, Map<String, String> headers);

    public ResponseEntity createUtilisateur(UtilisateurAPI utilisateurAPI, Map<String, String> headers);

    public ResponseEntity updateUtilisateur(UtilisateurAPI newUtilisateur, Map<String, String> headers);

    public ResponseEntity deleteById(String id, Map<String, String> headers);

    public Utilisateur convertFromUtilisateurAPIToUtilisateur(UtilisateurAPI utilisateurAPI, Utilisateur utilisateur);

    public ResponseEntity<List<Utilisateur>> findAll(Map<String, String> headers);

    public ResponseEntity<Utilisateur> findByloginAndMotPassse(String login, String motPasse, Map<String, String> headers);
}
