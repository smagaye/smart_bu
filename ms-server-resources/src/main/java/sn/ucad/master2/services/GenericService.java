package sn.ucad.master2.services;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.util.Map;


public interface GenericService {

    /**
     * @param headersMap la liste des paramètres de l'entête
     * @param method la métode http de la requête soumise
     * @return 200 si la requête est valide et 403 s'il n'y a pas d'autorisation
     */
    public HttpStatus isValidToken(Map<String, String> headersMap, HttpMethod method);

}
