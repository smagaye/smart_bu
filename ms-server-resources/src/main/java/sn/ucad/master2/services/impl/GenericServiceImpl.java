package sn.ucad.master2.services.impl;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import sn.ucad.master2.services.GenericService;

import java.util.Map;


@Service
public class GenericServiceImpl implements GenericService {

    @Value("${server.authorization.address}")
    private String serverAddress;

    @Value("${server.authorization.port}")
    private String serverPort;

    @Value("${server.authorization.url.test}")
    private String serverUrlTest;

    private String getUriTest() {
        String uri = serverAddress +":"+serverPort + serverUrlTest;
        return uri;
    }


    @Override
    public HttpStatus isValidToken(Map<String, String> headersMap,HttpMethod method) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        ResponseEntity response = null;

        headersMap.forEach((key, value) -> {
            headers.add(key,value);
        });

        HttpEntity entity = new HttpEntity(headers);
        try{
            response = restTemplate.exchange(
                    getUriTest(), method, entity, String.class);
        }
        catch (HttpClientErrorException e){
            return e.getStatusCode();
        }

        return response.getStatusCode();
    }
}
