package sn.ucad.master2.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.ucad.master2.bo.Utilisateur;
import sn.ucad.master2.dto.beans.UtilisateurAPI;
import sn.ucad.master2.exception.EntityNotFoundException;
import sn.ucad.master2.helpers.KeyGenerator;
import sn.ucad.master2.repositories.UtilisateurRepository;
import sn.ucad.master2.services.GenericService;

import java.util.List;
import java.util.Map;

@Service
public class UtilisateurServiceImpl implements sn.ucad.master2.services.UtilisateurService {

    @Autowired
    private GenericService genericService;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    /**
     * @param utilisateur
     * @return
     */
    private Utilisateur checkUtilisateur(UtilisateurAPI utilisateur){
        return utilisateurRepository.findBySlugOrTelephoneOrEmail(utilisateur.getSlug(),utilisateur.getTelephone(),utilisateur.getEmail());
    }

    /**
     * @param utilisateur
     */
    @Override
    public void delete(Utilisateur utilisateur){
        utilisateurRepository.delete(utilisateur);
    }

    /**
     * @param slug
     */
    @Override
    public void delete(int slug){
        utilisateurRepository.deleteById(slug);
    }

    /**
     * @param id
     * @return
     * @throws EntityNotFoundException
     */
    @Override
    public ResponseEntity<Utilisateur> findByPk(String id, Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers,HttpMethod.GET);
        if (httpStatusResponse.is2xxSuccessful())
            return new ResponseEntity<>(utilisateurRepository.findById(id),HttpStatus.OK);
        else
            return new ResponseEntity(httpStatusResponse);
    }

    /**
     * @param slug
     * @param headers
     * @return
     */
    @Override
    public ResponseEntity<Utilisateur>findById(String slug, Map<String, String> headers){
        HttpStatus httpStatusResponse = genericService.isValidToken(headers,HttpMethod.GET);
        if (httpStatusResponse.is2xxSuccessful())
            return new ResponseEntity<>(utilisateurRepository.findBySlug(Integer.parseInt(slug)),HttpStatus.OK);
        else
            return new ResponseEntity(httpStatusResponse);

    }


    @Override
    public ResponseEntity createUtilisateur(UtilisateurAPI utilisateurAPI, Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers, HttpMethod.POST);
        if(httpStatusResponse.is2xxSuccessful())
            try{
               Utilisateur utilisateur = checkUtilisateur(utilisateurAPI);
              if (utilisateur==null){
                  utilisateur = new Utilisateur();
                  convertFromUtilisateurAPIToUtilisateur(utilisateurAPI,utilisateur);
                  utilisateur.setId(KeyGenerator.id(utilisateurAPI.getProfile()));
                  utilisateurRepository.save(utilisateur);
              }else
                  httpStatusResponse=HttpStatus.CONFLICT;

            }catch (Exception e){
                httpStatusResponse = HttpStatus.NOT_ACCEPTABLE;
        }
        return new ResponseEntity(httpStatusResponse);
    }

    /**
     * @param newUtilisateur
     * @param headers
     * @return
     */
    @Override
    public ResponseEntity updateUtilisateur(UtilisateurAPI newUtilisateur, Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers, HttpMethod.POST);
        if(httpStatusResponse.is2xxSuccessful())
            try{
                Utilisateur oldUtilisateur = checkUtilisateur(newUtilisateur);
                if (oldUtilisateur!=null){
                    oldUtilisateur=convertFromUtilisateurAPIToUtilisateur(newUtilisateur,oldUtilisateur);
                    utilisateurRepository.save(oldUtilisateur);
                }else
                    httpStatusResponse=HttpStatus.NOT_MODIFIED;

            }catch (Exception e){
                httpStatusResponse = HttpStatus.BAD_REQUEST;
            }
        return new ResponseEntity(httpStatusResponse);
    }

    /**
     * @param id
     * @param headers
     * @return
     */
    @Override
    public ResponseEntity deleteById(String id, Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers, HttpMethod.POST);
        if(httpStatusResponse.is2xxSuccessful())
            try{
                utilisateurRepository.deleteById(Integer.parseInt(id));
                httpStatusResponse=HttpStatus.ACCEPTED;
            }catch (Exception e){
                httpStatusResponse = HttpStatus.NO_CONTENT;
            }
        return new ResponseEntity(httpStatusResponse);
    }

    /**
     * @param utilisateurAPI
     * @param utilisateur
     * @return
     */
    @Override
    public Utilisateur convertFromUtilisateurAPIToUtilisateur(UtilisateurAPI utilisateurAPI, Utilisateur utilisateur){
        utilisateur.setProfile(utilisateurAPI.getProfile()!=null ? utilisateurAPI.getProfile() : utilisateur.getProfile());
        utilisateur.setPrenom(utilisateurAPI.getPrenom()!=null ? utilisateurAPI.getPrenom() : utilisateur.getPrenom());
        utilisateur.setNationalite(utilisateurAPI.getNationalite()!=null ? utilisateurAPI.getNationalite() : utilisateur.getNationalite());
        utilisateur.setEmail(utilisateurAPI.getEmail()!=null ? utilisateurAPI.getEmail() : utilisateur.getEmail());
        utilisateur.setId(utilisateurAPI.getId()!=null ? utilisateurAPI.getId() : utilisateur.getId());
        utilisateur.setAdresse(utilisateurAPI.getAdresse()!= null ? utilisateurAPI.getAdresse() : utilisateur.getAdresse());
        utilisateur.setPhoto(utilisateurAPI.getPhoto()!= null ? utilisateurAPI.getPhoto() : utilisateur.getPhoto());
        utilisateur.setDateNaissance(utilisateurAPI.getDateNaissance()!=null ? utilisateurAPI.getDateNaissance() : utilisateur.getDateNaissance());
        utilisateur.setLieuNaissance(utilisateurAPI.getLieuNaissance()!=null ? utilisateurAPI.getLieuNaissance() : utilisateur.getLieuNaissance());
        utilisateur.setTelephone(utilisateurAPI.getTelephone()!=null ? utilisateurAPI.getTelephone() : utilisateur.getTelephone());
        utilisateur.setNom(utilisateurAPI.getNom()!=null ? utilisateurAPI.getNom() : utilisateur.getNom());
        utilisateur.setMotPasse(utilisateurAPI.getMotPasse()!=null ? utilisateurAPI.getMotPasse() : utilisateur.getMotPasse());

        return utilisateur;
    }

    @Override
    public ResponseEntity<List<Utilisateur>> findAll(Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers,HttpMethod.GET);
        if (httpStatusResponse.is2xxSuccessful())
            return new ResponseEntity<>(utilisateurRepository.findAll(),HttpStatus.OK);
        else
            return new ResponseEntity(httpStatusResponse);
    }

    @Override
    public ResponseEntity<Utilisateur> findByloginAndMotPassse(String login, String motPasse, Map<String, String> headers) {
        HttpStatus httpStatusResponse = genericService.isValidToken(headers,HttpMethod.GET);
        if (httpStatusResponse.is2xxSuccessful()){
            Utilisateur utilisateur = utilisateurRepository.findByEmailAndMotPasse(login, motPasse);
            return (utilisateur!=null)? new ResponseEntity<>(utilisateur,HttpStatus.OK) : new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
