package sn.ucad.master2.dto.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Setter;
import java.sql.Date;


@Setter
@ApiModel(description = "Toutes les informations à propos d'un utilisateur. ")
@JsonIgnoreProperties(value = { "motPasse" , "photo", "id"})
public class UtilisateurAPI {

    @ApiModelProperty(notes = "email de l'utilisateur") private String email;
    @JsonIgnoreProperties(value = { "id"}) private String id;
    @JsonIgnoreProperties(value = { "motPasse"}) private String motPasse;
    @ApiModelProperty(notes = "le numéro l'utilisateur") @JsonProperty("Id") private Integer slug;
    @ApiModelProperty(notes = "le prénom de l'utilisateur") private String prenom;
    @ApiModelProperty(notes = "le numéro de téléphone de l'utilisateur") private String telephone;
    @ApiModelProperty(notes = "le nom de l'utilisateur") private String nom;
    @ApiModelProperty(notes = "lieu de l'utilisateur")private String lieuNaissance;
    @ApiModelProperty(notes = "l'adresse de l'utilisateur")private String adresse;
    @ApiModelProperty(notes = "la date de naissance de l'utilisateur")private Date dateNaissance;
    @ApiModelProperty(notes = "la nationalité de l'utilisateur")private String nationalite;
    @ApiModelProperty(notes = "le profile de l'utilisateur")private String profile;
    @ApiModelProperty(notes = "le nom de la photo de l'utilisateur")private String photo;
    public String getId() {
        return this.id;
    }
    public Integer getSlug() {
        return this.slug;
    }
    public String getPrenom() {
        return this.prenom;
    }
    public String getNom() {
        return this.nom;
    }
    public String getEmail() {
        return this.email;
    }
    public String getMotPasse() {
        return this.motPasse;
    }
    public String getAdresse() {
        return this.adresse;
    }
    public String getTelephone() {
        return this.telephone;
    }
    public Date getDateNaissance() {
        return this.dateNaissance;
    }
    public String getLieuNaissance() {
        return this.lieuNaissance;
    }
    public String getNationalite() {
        return this.nationalite;
    }
    public String getProfile() {
        return this.profile;
    }
    public String getPhoto() {
        return this.photo;
    }
}