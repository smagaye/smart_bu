package com.smag.security.service;

import org.springframework.http.ResponseEntity;

public interface GenericService {

    public ResponseEntity<String> getResources(String url);

}
