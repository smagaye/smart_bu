package com.smag.security.service.impl;


import com.smag.security.service.GenericService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;


@Service
public class GenericServiceImpl implements GenericService {

    @Value("${server.resource.address}")
    private String serverAddress;

    @Value("${server.resource.port}")
    private String serverPort;

    @Override
    public ResponseEntity<String> getResources(String url){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = null;
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        HttpEntity entity = new HttpEntity(headers);
        try{

            response = restTemplate.exchange(
                    formatToURI(url), HttpMethod.GET, entity, String.class);
        }
        catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return response;
    }

    private String formatToURI(String url) {
        String uri = url == null ? serverAddress : serverAddress +":"+serverPort + url;
        System.out.println(uri);
        return uri;
    }


}
