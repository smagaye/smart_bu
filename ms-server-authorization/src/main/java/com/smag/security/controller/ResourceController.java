package com.smag.security.controller;

import com.smag.security.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api/v1.0/")
public class ResourceController {
    @Autowired
    private GenericService userService;

    @Autowired
    private TokenStore tokenStore;

    @RequestMapping(value ="status", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity isValidToken(OAuth2Authentication auth){
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value ="status", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity isValidTokenPost(OAuth2Authentication auth){
        return new ResponseEntity(HttpStatus.OK);
    }
}
